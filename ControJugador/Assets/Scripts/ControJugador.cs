﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControJugador : MonoBehaviour
{
    private Rigidbody rb;
    public int rapidez;
    public Text textoPuntaje;
    private int cont;



    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cont = 0;
        seteartexto(); 
    }

    private void seteartexto()
    {
        textoPuntaje.text="Pastillas tomadas:" + cont.ToString();

        if(cont>=5)
        {
            
        }
           
    }

    void FixedUpdate()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");

        Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);
        rb.AddForce(vectorMovimiento * rapidez);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coleccionable") == true)
        {
            cont = cont + 1;
            seteartexto();
            other.gameObject.SetActive(false);
        }

    }



}
